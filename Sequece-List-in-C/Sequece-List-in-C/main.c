//
//  main.c
//  Sequece-List-in-C
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <stdio.h>
#include "SequenceList.h"

void testSequenceList();

int main(int argc, const char * argv[]) {
    printf("testSequenceList\n");
    testSequenceList();
    return 0;
}

void testSequenceList() {
    SqList sqList;
    initList(&sqList, 5);
    
    insertElem(&sqList, 0, 1);
    insertElem(&sqList, 1, 2);
    insertElem(&sqList, 2, 3);
    insertElem(&sqList, 3, 4);
    insertElem(&sqList, 4, 5);
    
    listTraverse(&sqList);
    
    int e = 0;
    deleteElem(&sqList, 1, &e);
    printf("e: %d\n", e);
    
    listTraverse(&sqList);
    
    getElem(&sqList, 2, &e);
    printf("e: %d\n", e);
    
    clearList(&sqList);
    
    listTraverse(&sqList);
    
    deleteList(&sqList);
}
