//
//  SequenceList.c
//  Sequece-List-in-C
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <stdlib.h>
#include "SequenceList.h"

void initList(p_SqList l, int size) {
    l->m_pList = (int *)malloc(sizeof(int) * size);
    
    if (l->m_pList == NULL) {
        exit(-1);
    }
    l->m_iSize = size;
    clearList(l);
}

void deleteList(p_SqList l) {
    free(l->m_pList);
    l->m_pList = NULL;
}

void clearList(p_SqList l) {
    l->m_iLen = 0;
}

int isEmpty(p_SqList l) {
    return l->m_iLen == 0;
}

int isFull(p_SqList l) {
    return l->m_iLen == l->m_iSize;
}

int listLen(p_SqList l) {
    return l->m_iLen;
}

int getElem(p_SqList l, int i, int *e) {
    if (i < 0 || i >= l->m_iLen) {
        return 0;
    }
    
    *e = l->m_pList[i];
    return 1;
}

int locateElem(p_SqList l, int e) {
    for (int i = 0; i < l->m_iLen; i ++) {
        if (l->m_pList[i] == e) {
            return i;
        }
    }
    
    return -1;
}

int priorElem(p_SqList l, int curElem, int *preElem) {
    int t = locateElem(l, curElem);
    if (t == -1 || t == 0) {
        return 0;
    }
    
    getElem(l, t - 1, preElem);
    return 1;
}

int nextElem(p_SqList l, int curElem, int *nextElem) {
    int t = locateElem(l, curElem);
    if (t == -1 || t == l->m_iLen - 1) {
        return 0;
    }
    
    getElem(l, t + 1, nextElem);
    return 1;
}

int insertElem(p_SqList l, int i, int elem) {
    if (isFull(l) || i < 0 || i > l->m_iLen) {
        return 0;
    }
    
    for (int j = l->m_iLen - 1; j >= i; j --) {
        l->m_pList[j + 1] = l->m_pList[j];
    }
    
    l->m_pList[i] = elem;
    l->m_iLen ++;
    return 1;
}

int deleteElem(p_SqList l, int i, int *elem) {
    if (isEmpty(l) || i < 0 || i > l->m_iLen) {
        return 0;
    }
    
    *elem = l->m_pList[i];
    
    for (int j = i; j < l->m_iLen - 1; j ++) {
        l->m_pList[j] = l->m_pList[j + 1];
    }
    
    l->m_iLen --;
    return 1;
}

void listTraverse(p_SqList l) {
    for (int i = 0; i < l->m_iLen; i ++) {
        printf("%d ", l->m_pList[i]);
    }
    
    printf("\n");
}
