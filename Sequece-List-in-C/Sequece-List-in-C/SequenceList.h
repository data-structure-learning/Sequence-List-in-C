//
//  SequenceList.h
//  Sequece-List-in-C
//
//  Created by 买明 on 22/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef SequenceList_h
#define SequenceList_h

#include <stdio.h>

typedef struct {
    int *m_pList;
    int m_iSize;
    int m_iLen;
} SqList, *p_SqList;

void initList(p_SqList l, int size);
void deleteList(p_SqList l);
void clearList(p_SqList l);
int isEmpty(p_SqList l);
int isFull(p_SqList l);
int listLen(p_SqList l);
int getElem(p_SqList l, int i, int *e);
int locateElem(p_SqList l, int e);
int priorElem(p_SqList l, int curElem, int *preElem);
int nextElem(p_SqList l, int curElem, int *nextElem);
int insertElem(p_SqList l, int i, int elem);
int deleteElem(p_SqList l, int i, int *elem);
void listTraverse(p_SqList l);

#endif /* SequenceList_h */
